#include <string.h>
#include <inttypes.h>
#include <openssl/sha.h>
#include "Gost.h"
#include "Signer.h"

void SignDigest (const std::vector<SignerPtr>& signers, const BIGNUM * digest, BIGNUM * r1, BIGNUM * s1)
{	
	EC_POINT * Q = CalculateMultisigPublic (signers);
	EC_POINT * C = CalculateMultisigC (signers);
	BIGNUM * mu = BN_new ();
	BN_rand_range (mu, curve->GetQ ()); 
	BIGNUM * epsilon = BN_new ();
	BN_rand_range (epsilon, curve->GetQ ()); 	   	

	BN_CTX * ctx = BN_CTX_new ();
	EC_POINT * C1 = EC_POINT_new (curve->GetGroup ());	
	EC_POINT_mul (curve->GetGroup (), C1, epsilon, Q, mu, ctx); // C1 = mu*Q + epsilon*G
	EC_POINT_add (curve->GetGroup (), C1, C, C1, ctx); // C1 = C + C1
	curve->GetXY (C1, r1, nullptr); // r1 = C1x
	EC_POINT_free (C1);
	EC_POINT_free (Q);
	EC_POINT_free (C);

	BIGNUM * h = BN_new ();
	BN_mod (h, digest, curve->GetQ (), ctx); // h = digest % q
    BIGNUM * e = BN_new ();    
	BN_mod_inverse (e, h, curve->GetQ (), ctx); // e = 1/h mod q
	BIGNUM * r = BN_new ();	
	BN_mod_mul (r, r1, e, curve->GetQ (), ctx); // r = r1*e = r1/h
	BN_mod_add (r, r, mu, curve->GetQ (), ctx); // r = r1/h + mu
	BIGNUM * s = MultiSign (signers, r); // actual signing
	BN_free (r);
	BN_mod_add (s, s, epsilon, curve->GetQ (), ctx); // s = s + epsilon
	BN_mod_mul (s1, h, s, curve->GetQ (), ctx); // s = h*s
	// here we have signature (r1, s1)
	BN_free (s);
	BN_free (h);
    BN_free (e);    	
	BN_CTX_free (ctx);
}


int main ()
{
    std::vector<SignerPtr> signers;
    for (int i = 0; i < 16; i++) // num signers
        signers.emplace_back (new Signer ());

	uint8_t text[100], digest[64];
	memset (text, 0, 100);
	SHA512 (text, 100, digest); // TODO: use GOST 34.11	
	BIGNUM * d = BN_bin2bn (digest, 64, nullptr);
	BIGNUM * r1 = BN_new (), * s1 = BN_new ();
	SignDigest (signers, d, r1, s1);
	// verify
	EC_POINT * Q = CalculateMultisigPublic (signers);	
	printf ("%i\n", curve->Verify (Q, d, r1, s1));
	BN_free (d);
	BN_free (r1);
	BN_free (s1);
	EC_POINT_free (Q);
}
