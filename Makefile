INCLUDEPATHS=-I.
DEFS=
CXXFLAGS= -g -Wall -std=c++11 $(DEFS) $(INCLUDEPATHS)
LDFLAGS=
LIBS=-lcrypto

OBJS=main.o Gost.o Signer.o

all: multisig

%.o: %.cpp 
	g++ $(CXXFLAGS) -c -o $@ $<

multisig: $(OBJS) 
	g++ $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	-rm *.o multisig
