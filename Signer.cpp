#include <vector>
#include "Gost.h"
#include "Signer.h"

std::unique_ptr<GOSTR3410Curve>& curve = GetGOSTR3410Curve (eGOSTR3410TC26A512); // set curve here

Signer::Signer ()
{
	// create keys
	priv = BN_new ();
	BN_rand_range (priv, curve->GetQ ()); 
	pub = curve->MulP (priv);
	// create random k and calculate C
	k = BN_new ();
	BN_rand_range (k, curve->GetQ ()); 
	C = curve->MulP (k);	
}

Signer::~Signer ()
{
	BN_free (priv);
	BN_free (k);
	EC_POINT_free (pub);
	EC_POINT_free (C);	
}	

BIGNUM * Signer::Sign (const BIGNUM * r, BN_CTX * ctx) const
{
	BIGNUM * s = BN_new ();
	BN_mod_mul (s, r, priv, curve->GetQ (), ctx);
	BN_mod_add (s, s, k, curve->GetQ (), ctx); 	
	return s;
}	

// multisig 


EC_POINT * CalculateMultisigPublic (const std::vector<SignerPtr>& signers)
{
	auto numSigners = signers.size ();
	if (numSigners < 2)
	{ 
		if (numSigners == 1) return EC_POINT_dup (signers[0]->GetPublic (), curve->GetGroup ());
		return nullptr;
	}
	auto res = EC_POINT_new (curve->GetGroup ());	
	BN_CTX * ctx = BN_CTX_new ();
	EC_POINT_add (curve->GetGroup (), res, signers[0]->GetPublic (), signers[1]->GetPublic (), ctx);
	for (size_t i = 0; i < numSigners - 2; i++)
		EC_POINT_add (curve->GetGroup (), res, res, signers[i+2]->GetPublic (), ctx);
	BN_CTX_free (ctx);	
	return res;
}

EC_POINT * CalculateMultisigC (const std::vector<SignerPtr>& signers)
{
	auto numSigners = signers.size ();
	if (numSigners < 2)
	{ 
		if (numSigners == 1) return EC_POINT_dup (signers[0]->GetPublic (), curve->GetGroup ());
		return nullptr;
	}

	auto res = EC_POINT_new (curve->GetGroup ());	
	BN_CTX * ctx = BN_CTX_new ();
	EC_POINT_add (curve->GetGroup (), res, signers[0]->GetC (), signers[1]->GetC (), ctx);
	for (size_t i = 0; i < numSigners - 2; i++)
		EC_POINT_add (curve->GetGroup (), res, res, signers[i+2]->GetC (), ctx);
	BN_CTX_free (ctx);	
	return res;
}

BIGNUM * MultiSign (const std::vector<SignerPtr>& signers, const BIGNUM * r)
{
	auto numSigners = signers.size ();
	if (numSigners < 1) return nullptr; 
	BN_CTX * ctx = BN_CTX_new ();
	BIGNUM * s = signers[0]->Sign (r, ctx);
	if (numSigners == 1)
	{
		BN_CTX_free (ctx);
		return s;
	}
	BIGNUM * s1 = signers[1]->Sign (r, ctx); 
	BN_mod_add (s, s, s1, curve->GetQ (), ctx); 
	for (size_t i = 0; i < numSigners - 2; i++)	
	{
		BN_free (s1);
		s1 = signers[i+2]->Sign (r, ctx);
		BN_mod_add (s, s, s1, curve->GetQ (), ctx); 
	}	
	BN_free (s1);
	BN_CTX_free (ctx);
	return s;
}

