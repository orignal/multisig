#include <vector>
#include "Gost.h"

extern std::unique_ptr<GOSTR3410Curve>& curve;
class Signer
{
	public:

		Signer ();
		~Signer ();		

		const EC_POINT * GetPublic () const { return pub; };  
		const EC_POINT * GetC () const { return C; };

		BIGNUM * Sign (const BIGNUM * r, BN_CTX * ctx) const;

	private:

		BIGNUM * priv, * k;
		EC_POINT * pub, * C;
};
typedef std::unique_ptr<Signer> SignerPtr;

// multisig

EC_POINT * CalculateMultisigPublic (const std::vector<SignerPtr>& signers);
EC_POINT * CalculateMultisigC (const std::vector<SignerPtr>& signers);
BIGNUM * MultiSign (const std::vector<SignerPtr>& signers, const BIGNUM * r);
